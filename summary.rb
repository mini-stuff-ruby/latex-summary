#!/usr/bin/env ruby

class String
  # colorization
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end

  def red
    colorize(31)
  end

  def green
    colorize(32)
  end

  def yellow
    colorize(33)
  end

  def blue
    colorize(34)
  end

  def pink
    colorize(35)
  end

  def light_blue
    colorize(36)
  end
end

def get_input_filename
  unless ARGV.empty?
    filename = ARGV.shift
    return filename unless filename == '.'
  end

  log_files = Dir['*.log']
  
  raise "No log file" if log_files.empty?
  
  log_files.sort! {|x,y| File.mtime(y) <=> File.mtime(x)}
  
  return log_files[0]
end

def die(msg)
  $stderr.puts(msg)
  exit(1)
end

unparsed_warning = Array.new
undefined_labels = Array.new
multiple_def_labels = Array.new

rerun = false

input_filename = get_input_filename

if ! File.exists?(input_filename)
  die("Input log file '#{input_filename}' not found")
end

return_status_to_shell = ARGV.empty?

IO.readlines(input_filename).each do |line|
  line = line.encode('UTF-8', invalid: :replace, replace: "")

  next unless line =~ /^LaTeX Warning: (.*)$/

  warning_body=$1
  
  case warning_body
  when /^Citation `([^']*)' on page [0-9]+ undefined/
    undefined_labels << "#{$1} (bibliography)"
    
  when /^Reference `([^']*)' on page [0-9]+ undefined on input line/
    undefined_labels << $1

  when /^Label `([^']*)' multiply defined/
    multiple_def_labels << $1

  when /^There were undefined references/
    raise "There were undefined references?" if undefined_labels.empty?

  when /^Label\(s\) may have changed. Rerun to get cross-references right/
    rerun=true

  else
    unparsed_warning << warning_body
  end
end

return_status = 0

Undefined_Status=2
Multiple_Defined_Status = 4
Rerun_Status = 8
Other_Status = 16

unless undefined_labels.empty?
  undefined_labels.sort!
  undefined_labels.uniq!

  puts "Undefined labels:".yellow
  undefined_labels.each do |label|
    puts "    #{label}".green
  end

  puts
  
  return_status += Undefined_Status
end

unless multiple_def_labels.empty?
  multiple_def_labels.sort!
  multiple_def_labels.uniq!

  puts "Muiltiple defined labels:".yellow
  multiple_def_labels.each do |label|
    puts "    #{label}".green
  end

  puts
  
  return_status += Multiple_Defined_Status
end

if rerun
  puts "Label changed, rerun".red
  puts
  
  return_status += Rerun_Status
end

unless unparsed_warning.empty?
  puts "Other warnings:".yellow
  unparsed_warning.each {|w| puts "   #{w}".blue }

  puts
  
  return_status += Other_Status
end


exit(return_status_to_shell ? return_status : 0)

